package com.example.a3ld;

import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        populateSpinner();
        populateAutoCompleteTextView();
    }

    private void populateSpinner() {
        Spinner spinner = findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.savaites_dienos, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void populateAutoCompleteTextView() {
        String[] fakultetai = getResources().getStringArray(R.array.fakultetai);
        AutoCompleteTextView autoCompleteTextView = findViewById(R.id.autoCompleteTextView);
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, fakultetai);
        autoCompleteTextView.setAdapter(adapter);
    }

    public void onSaugoti(View view) {
        EditText editText = findViewById(R.id.editText);
        String pavadinimas = editText.getText().toString();

        AutoCompleteTextView autoCompleteTextView = findViewById(R.id.autoCompleteTextView);
        String fakultetas = autoCompleteTextView.getText().toString();

        RatingBar ratingBar = findViewById(R.id.ratingBar);
        String reitingas = String.valueOf((int) ratingBar.getRating());

        Spinner spinner = findViewById(R.id.spinner);
        Object diena = spinner.getSelectedItem();

        TimePicker timePicker = findViewById(R.id.timePicker);
        String laikas = timePicker.getCurrentHour() + ":" + timePicker.getCurrentMinute();

        Switch theSwitch = findViewById(R.id.switch3);
        String arRegistruoti = theSwitch.isChecked() ? "Taip" : "Ne";

        String output = "Pavadinimas: " + pavadinimas +
                "\nFakultetas: " + fakultetas +
                "\nReitingas: " + reitingas +
                "\nDiena: " + diena +
                "\nLaikas:" + laikas +
                "\nRegistruoti: " + arRegistruoti;

        Toast.makeText(this, output, Toast.LENGTH_SHORT).show();
    }
}